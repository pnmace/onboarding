package onboarding.week1;

public class ListIntersection {
	
	public static void main(String[] args) {

		LinkedList<Integer> linkedlist1 = new LinkedList<Integer>();
		LinkedList<Integer> linkedlist2 = new LinkedList<Integer>();
		Node<Integer> dupe = new Node<Integer>(8);
		dupe.next = new Node<Integer>(10);
		linkedlist1.add(3);linkedlist1.add(7);linkedlist1.add(dupe);linkedlist1.add(35);
		linkedlist2.add(99);linkedlist2.add(1);linkedlist2.add(dupe);linkedlist2.add(12);
		
		Node<?> node = checkNodes(linkedlist1.head, linkedlist2.head);

		System.out.println(node.val);
		System.out.println(node.next);
	}
	
	private static Node<?> checkNodes(Node<?> list1, Node<?> list2) {
		while (list1 != null && list2 != null) {
			if (list1.equals(list2)) return list1;
			list1 = list1.next;
			list2 = list2.next;
		}
		
		return null;
	}
	
	private static class LinkedList<T> {
		Node<T> head = null;
		Node<T> tail = null;
		
		void add(T val) {
			if (head == null) {
				head = new Node<T>(val);
				tail = head;
				head.next = tail;
			} else {
				Node<T> temp = tail;
				tail = new Node<T>(val);
				temp.next = tail;
			}
		}
		
		void add(Node<T> val) {
			if (head == null) {
				head = val;
				tail = head.next;
			} else {
				Node<T> temp = tail;
				tail = val.next;
				temp.next = val;
			}
		}
	}
	
	private static class Node<T> {
		Node(T val) {
			this.val = val;
		}
		T val;
		Node<T> next;
	}

}
