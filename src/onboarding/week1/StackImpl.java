package onboarding.week1;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;

public class StackImpl {

	public static void main(String[] args) {
		
		MyStack<Double> stack = new MyStack<Double>();

		stack.push(40.2);
		stack.push(30.2);
		stack.push(80.2342341);
		stack.push(80.234234);
		stack.push(30.23);
		stack.push(80.42);
		stack.push(80.32);

		for (var i : stack.list)
			System.out.println(i);
		
		System.out.println("\nMax val:" + stack.max() + "\n");
		
		stack.pop();
		stack.pop();
		stack.pop();
		
		for (var i : stack.list)
			System.out.println(i);

		System.out.println("\nMax val:" + stack.max() + "\n");
	}
	
	private static class MyStack<T extends Number> {
		List<T> list = new ArrayList<T>();
		List<T> stack = new ArrayList<T>();
		
		void push(T val) {
			if (stack.size() == 0) {
				stack.add(val);
				list.add(val);
			}
			
			if (val.doubleValue() >= stack.get(stack.size()-1).doubleValue()) stack.add(val);
			
			list.add(val);
		}
		
		T pop() {
			if (list.size() == 0) throw new EmptyStackException();
			
			T item = list.get(list.size()-1);
			list.remove(list.size()-1);
			
			if (item.doubleValue() == stack.get(stack.size()-1).doubleValue()) stack.remove(stack.size()-1);
			
			return item;
		}
		
		T max() {
			if (stack.size() == 0) throw new EmptyStackException();
			
			return stack.get(stack.size()-1);
		}

	}
	
}
