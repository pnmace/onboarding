pub mod circular_array {
    pub struct RotatedArray<T> {
        array: Vec<T>,
        rotation: i32,
    }

    impl<T> RotatedArray<T> {
        pub fn rotated_array(array: Vec<T>, rotation: i32) -> RotatedArray<T> {
            RotatedArray::<T> {
                array: array,
                rotation: rotation
            }
        }

        pub fn push(&mut self, value: T) {
            self.array.push(value);
        }

        pub fn rotate(&mut self, value: i32) {
            self.rotation = (self.rotation + value) % 360;
        }
    
        pub fn get_rotation(&self) -> i32 {
            self.rotation
        }

        pub fn get_array(&self) -> &Vec<T> {
            &self.array
        }
    }
}