pub use circular_array::circular_array::RotatedArray;

fn main() {

    let array = vec![1,2,3,4,5];

    let mut circular = RotatedArray::rotated_array(array, 0);

    circular.push(10);

    circular.rotate(400);
    circular.rotate(-200);

    println!("the array is now rotated to {} degrees", circular.get_rotation());

    for val in circular.get_array().iter() {
        println!("{}", val);
    }

}
