fn main() {
    // 1 = open; 0 = closed
    let mut lockers: Vec<u8> = vec![1; 100];
    
    let mut i = 1;

    while i < 50 {
        i += 1;
        for j in (i-1..101).step_by(i.into()) {
            lockers[j] ^= 1;
        }
    }

    for j in 51..101 {
        lockers[j-1] ^= 1;
    }

    let mut ans = 0;

    for num in lockers {
        if num == 1 { ans += 1; }
    }

    println!("{}", ans);
}                          
